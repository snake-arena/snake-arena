#include "snake_bot.h"

using namespace snake;

path_data snake::bot::get_path_data(){
  path_data p;
  p.row = row;
  p.col = col;
  p.dir = direction;
  return p;
}
