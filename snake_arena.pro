######################################################################
# Automatically generated by qmake (2.01a) ons jul 9 18:32:24 2014
######################################################################

TEMPLATE = app
TARGET = 
DEPENDPATH += .
INCLUDEPATH += .
QMAKE_CXXFLAGS += -O3 -std=c++11
CONFIG += qt debug
CONFIG += c++11
# LIBS += -ldl

# target.path = /usr/local/kaos
# documentation.path = /usr/local/kaos/doc
# documentation.files = docs/*
# INSTALLS += target

# Input
HEADERS += snake_interface.h \
snake_bot.h \
snake_utility.h \
snake_extra.h \
snake_vis.h

SOURCES += main.cpp\
 snake_interface.cpp\
 snake_extra.cpp\
 snake_vis.cpp\
 snake_bot.cpp

