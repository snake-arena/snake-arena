#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <unistd.h>

#include "snake_extra.h"

using namespace std;

ostream& operator << (ostream &stream, const QString &str){
  stream << str.toStdString();
  return stream;
}

const char* q2cstring(QString v){
  return v.toUtf8().data();
}

time_type time_millis(){ 
  return chrono::duration_cast<chrono::milliseconds> (chrono::system_clock::now().time_since_epoch()).count();
}

int get_pid(const char* pname){
  int ret = -1;
  char query[256];
  char line[1024];

  sprintf(query, "ps -e | grep %s", pname);
  FILE *f = popen(query, "r");

  if (!f){
    cout << "get_pid: query: " << query << ", failed to open pipe." << endl;
    exit(-1);
  }else if (fgets(line, 1024, f)){
    stringstream ss(line);
    ss >> ret;
    cout << "get_pid: query: " << query << ":  line was " << line << ", return is " << ret << endl;
  }else{
    cout << "get_pid: query: " << query << ":  no data in pipe." << endl;
  }

  pclose(f);
  
  return ret;
}
