#ifndef SNAKE_INTERFACE
#define SNAKE_INTERFACE

#include <vector>
#include <QtGui>
#include "snake_bot.h"
#include "snake_extra.h"
#include "snake_vis.h"

namespace snake{
  class interface : public QDialog{
    Q_OBJECT;
  private:
    QPushButton *input_add_bots;
    QPushButton *input_clear_bots;
    QPushButton *input_start;
    QPushButton *input_abort;
    QPushButton *input_forward;
    QPushButton *input_backward;
    QLineEdit *display_step;
    QLineEdit *input_rows;
    QLineEdit *input_cols;
    QLineEdit *input_framerate;
    vis *image_buf;
    QListWidget *show_bots;
    float frame_rate;

    const QString com_root = "/mnt/ram";
    const QString com_file = "map.buf";
    const QString window_title = "SNAKE ARENA";
    const int init_margin = 1;
    const int max_bots = 3;
    const char* ts_masks[4] = {"1","2","4","8"};
    
    void cache_images();
    void setup_bots(QStringList files);
    void update_map();
    void write_state();
    void paint_map();
    void update_bot_list();
    void update_replay();
    int count_bots();
    void display_winner();
    void setup();
    void set_control_enables(bool x);

  protected:
    void closeEvent(QCloseEvent *e);
    void resizeEvent(QResizeEvent *e);

  public:
    std::vector<bot> bots;
    int rows;
    int cols;
    std::vector<int> map_buf;
    time_type next_epoch;
    int game_over;
    // index:
    // 1: snake theme
    // 2: snake section
    // 3: rotation
    std::map<QString, QImage> image_data;
    const int display_width = 600;
    const int display_height = 600;
    const bool use_images = true;
    const QString section_names[4] = {"head", "body", "tail", "corner"};
    int replay_step;
    int replay_max;

    interface();
  
    private slots:
      void add_bots();
      void clear_bots();
      void start();
      void abort();
      void step_forward();
      void step_backward();
      void update_rows();
      void update_cols();
  };
};
#endif
