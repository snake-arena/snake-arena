#include <iostream>
#include "snake_vis.h"
#include "snake_extra.h"
#include "snake_interface.h"
#include "snake_utility.h"

using namespace std;

QPolygonF make_triangle(snake::bot_direction d, float x, float y, float w, float h){
  QVector<QPointF> points;

  switch(d){
  case snake::north:
    points.append(QPointF(x, y + h));
    points.append(QPointF(x + w/2, y));
    points.append(QPointF(x + w, y + h));
    break;
  case snake::east:
    points.append(QPointF(x, y));
    points.append(QPointF(x + w, y + h/2));
    points.append(QPointF(x, y + h));
    break;
  case snake::south:
    points.append(QPointF(x + w, y));
    points.append(QPointF(x + w/2, y + h));
    points.append(QPointF(x, y));
    break;
  case snake::west:
    points.append(QPointF(x + w, y));
    points.append(QPointF(x + w, y + h));
    points.append(QPointF(x, y + h/2));
    break;
  }

  return QPolygonF(points);
}

snake::vis::vis(snake::interface *f) : QLabel(){
  iface = f;
}

int snake::vis::heightForWidth(int w) const{
  if (iface -> rows && iface -> cols){
    return (w * iface -> rows) / iface -> cols;
  }else{
    return w;
  }
}

int snake::vis::widthForHeight(int h) const{
  if (iface -> rows && iface -> cols){
    return (h * iface -> cols) / iface -> rows;
  }else{
    return h;
  }
}

void snake::vis::check_size(){
  QResizeEvent e(size(), size());
  resizeEvent(&e);
}

void snake::vis::resizeEvent(QResizeEvent *e){
  int h = e -> size().height();
  int w = e -> size().width();
  int hfw = heightForWidth(w);

  cout << "resize called with " << w << "x" << h << ", hfw is " << hfw << endl;
  
  if (h < hfw){
    int wn = widthForHeight(h);
    cout << "h<hfw: calling recursive: " << wn << "x" << h << endl;
    resize(wn, h);
    return;
  }else if(h > hfw){
    cout << "h>hfw: calling recursive: " << w << "x" << hfw << endl;
    resize(w, hfw);
    return;
  }

  QLabel::resizeEvent(e);
}

void snake::vis::paintEvent(QPaintEvent *e){
  QPainter q;
  int r,c,i;
  int rows = iface -> rows;
  int cols = iface -> cols;
  int num = rows * cols;
  QString key;

  QLabel::paintEvent(e);
  
  if (iface -> map_buf.size() == num){
    float rw = width() / (float)cols;
    float rh = height() / (float)rows;

    q.begin(this);

    if (iface -> use_images){
      // paint grid
      q.setPen(color_grid);
      // q.setBrush(color_player[0]);
      for (i = 0; i < num; i++){
	r = i % rows;
	c = i / rows;
	q.drawRect(c * rw, r * rh, rw, rh);
      }
    }else{
      // paint grid
      q.setPen(color_grid);
      for (i = 0; i < num; i++){
	r = i % rows;
	c = i / rows;
	q.setBrush(color_player[iface -> map_buf[r + iface -> rows * c]]);
	q.drawRect(c * rw, r * rh, rw, rh);
      }
    }

    // paint bots
    for (i = 0; i < iface -> bots.size(); i++){
      if (iface -> use_images){
	list<path_data> path;

	if (iface -> replay_max){
	  auto buf = iface -> bots[i].path;
	  for (int j = 0; j <= iface -> replay_step && buf.size(); j++){
	    path.push_back(buf.front());
	    buf.pop_front();
	  }
	}else{
	  path = iface -> bots[i].path;
	}

	auto it_head = path.end();
	it_head--;

	// draw tail
	if (path.size() > 1){
	  r = path.front().row;
	  c = path.front().col;
	  auto it_dir = path.begin();
	  it_dir++;
	  key = QString::number(i) + "2" + QString::number(it_dir -> dir);
	  q.drawImage(c * rw, r * rh, iface -> image_data[key]);
	}

	// draw body
	auto j = path.begin();
	j++; // skip tail
	for (; j != it_head && j != path.end(); j++){
	  auto k = j;
	  k++;
	  int dnext = k -> dir;
	  int dcur = j -> dir;
	  int ddif = dnext == (dcur + 5) % 4 ? 1 : (dnext == (dcur + 3) % 4 ? -1 : 0);
	  r = j -> row;
	  c = j -> col;
	  // tf.reset();
	  if (ddif){
	    if (ddif == 1){
	      // img = iface -> image_corner[i].transformed(tf.rotate(90 * j -> dir));
	      key = QString::number(i) + "3" + QString::number(j -> dir);
	    }else{
	      // img = iface -> image_corner[i].transformed(tf.rotate(90 * (j -> dir + 1)));
	      key = QString::number(i) + "3" + QString::number((j -> dir + 1) % 4);
	    }
	  }else{
	    // img = iface -> image_body[i].transformed(tf.rotate(90 * j -> dir));
	    key = QString::number(i) + "1" + QString::number(j -> dir);
	  }
	  q.drawImage(c * rw, r * rh, iface -> image_data[key]);
	}
	it_head = j == path.end() ? path.begin() : j;

	// draw head
	r = it_head -> row;
	c = it_head -> col;
	// tf.reset();
	// img = iface -> image_head[i].transformed(tf.rotate(90 * it_head -> dir));
	key = QString::number(i) + "0" + QString::number(it_head -> dir);
	q.drawImage(c * rw, r * rh, iface -> image_data[key]);

      }else{
	q.setBrush(color_player[i+1]);
	q.drawConvexPolygon(make_triangle(iface -> bots[i].direction, iface -> bots[i].col * rw, iface -> bots[i].row * rh, rw, rh));
      }
    }

    q.end();
  }else{
    cout << "vis: incorrect map_buf.size(): is " << iface -> map_buf.size() << ", should be " << num << endl;
  }
}
