#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <unistd.h>

#include "snake_interface.h"
#include "snake_utility.h"
#include "snake_extra.h"

using namespace std;

typedef pair<int,int> ptype;
ptype neighbours[4] = {ptype(-1,0), ptype(0,1), ptype(1,0), ptype(0,-1)};

snake::interface::interface(){
  QHBoxLayout *layout_root = new QHBoxLayout();
  QVBoxLayout *layout_control = new QVBoxLayout();
  QGroupBox *box_control = new QGroupBox();

  rows = 9;
  cols = 9;
  replay_max = 0;

  // build interface
  input_add_bots = new QPushButton("add bots");
  connect(input_add_bots, SIGNAL(clicked()), this, SLOT(add_bots()));
  layout_control -> addWidget(input_add_bots);

  input_clear_bots = new QPushButton("clear bots");
  connect(input_clear_bots, SIGNAL(clicked()), this, SLOT(clear_bots()));
  layout_control -> addWidget(input_clear_bots);

  QLabel *l_load = new QLabel("loaded bots:", this);
  l_load -> setMaximumHeight(20);
  layout_control -> addWidget(l_load);

  show_bots = new QListWidget(this);
  show_bots -> setIconSize(QSize(40,40));
  show_bots -> setSelectionMode(QAbstractItemView::NoSelection);
  layout_control -> addWidget(show_bots);

  input_rows = new QLineEdit();
  input_rows -> setPlaceholderText("rows: 9");
  connect(input_rows, SIGNAL(editingFinished()), this, SLOT(update_rows()));
  layout_control -> addWidget(input_rows);

  input_cols = new QLineEdit();
  input_cols -> setPlaceholderText("cols: 9");
  connect(input_cols, SIGNAL(editingFinished()), this, SLOT(update_cols()));
  layout_control -> addWidget(input_cols);

  input_framerate = new QLineEdit();
  input_framerate -> setPlaceholderText("framerate: 2");
  layout_control -> addWidget(input_framerate);

  input_start = new QPushButton("start");
  connect(input_start, SIGNAL(clicked()), this, SLOT(start()));
  input_start -> setEnabled(false);
  layout_control -> addWidget(input_start);

  input_abort = new QPushButton("abort");
  connect(input_abort, SIGNAL(clicked()), this, SLOT(abort()));
  input_abort -> setEnabled(false);
  layout_control -> addWidget(input_abort);

  input_backward = new QPushButton("<<");
  input_backward -> setEnabled(false);
  connect(input_backward, SIGNAL(clicked()), this, SLOT(step_backward()));
  layout_control -> addWidget(input_backward);

  input_forward = new QPushButton(">>");
  input_forward -> setEnabled(false);
  connect(input_forward, SIGNAL(clicked()), this, SLOT(step_forward()));
  layout_control -> addWidget(input_forward);

  display_step = new QLineEdit("replay: 0");
  display_step -> setEnabled(false);
  layout_control -> addWidget(display_step);

  box_control -> setLayout(layout_control);
  box_control -> setMaximumWidth(250);
  // box_control -> setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Ignored);

  layout_root -> addWidget(box_control);
  
  image_buf = new snake::vis(this);
  image_buf -> resize(display_width, display_height);

  QSizePolicy qsp(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
  // todo: fix hfw
  // qsp.setHeightForWidth(true);
  image_buf -> setSizePolicy(qsp);
  layout_root -> addWidget(image_buf);
  // layout_root -> setAlignment(image_buf, Qt::AlignRight);

  // object names
  show_bots -> setObjectName("show_bots");
  image_buf -> setObjectName("image_buf");

  setMinimumSize(800, 600);
  setLayout(layout_root);
  setWindowTitle(window_title);
  setVisible(true);
}

void snake::interface::cache_images(){
  if (!(rows && cols)){
    return;
  }

  QTransform tf;
  int w = image_buf -> width() / cols;
  int h = image_buf -> height() / rows;

  setWindowTitle("LOADING...");
  update();
  QCoreApplication::processEvents();

  for (int theme = 0; theme < max_bots; theme++){
    for (int section = 0; section < 4; section ++){
      for (int rotation = 0; rotation < 4; rotation++){
	QImage img("data/snake-" + section_names[section] + "-" + QString::number(theme) + ".png");
	tf.reset();
	tf.rotate(90 * rotation);
	image_data[QString::number(theme) + QString::number(section) + QString::number(rotation)] = img.transformed(tf).scaled(w,h);
      }
    }
  }

  setWindowTitle(window_title);
}

void snake::interface::set_control_enables(bool x){
  input_start -> setEnabled(x);
  input_add_bots -> setEnabled(x);
  input_clear_bots -> setEnabled(x);
  input_forward -> setEnabled(x);
  input_backward -> setEnabled(x);
}

void snake::interface::clear_bots(){
  bots.clear();
  system(q2cstring("rm " + com_root + "/*.scm"));
  update_bot_list();
}

void snake::interface::add_bots(){
  QFileDialog fd(this, QString("Select bot exec files"));
  fd.setFileMode(QFileDialog::ExistingFiles);
  if (fd.exec()){
    setup_bots(fd.selectedFiles());
  }
}

void snake::interface::setup_bots(QStringList files){
  int i;
  int buf = snake::forward;
  int num_old = bots.size();
  snake::bot bot_buf;

  if (num_old + files.length() > max_bots){
    QMessageBox(QMessageBox::Information, "Bot limit", "Can not have more than " + QString::number(max_bots) + " bots!").exec();
    return;
  }

  bots.resize(num_old + files.length());
  for (i = 0; i < files.length(); i++){
    QFileInfo fi(files[i]);
    if (!(fi.permissions() & QFile::ExeOther)){
      cout << "The file " << files[i].toStdString() << " lacks global exec permissions!" << endl;
      bots.resize(num_old);
      update_bot_list();
      return;
    }
    bot_buf.exec_file = fi.fileName();
    bot_buf.exec_path = fi.filePath();
    if (bot_buf.exec_path.length() < 1){
      bot_buf.exec_path = ".";
    }
    bot_buf.com_file = fi.baseName() + ".scm";
    cout << "loaded bot: '" << bot_buf.exec_file << "' @ '" << bot_buf.exec_path << "', com = '" << bot_buf.com_file << "'." << endl;

    fstream f(q2cstring(com_root + "/" + bot_buf.com_file), ios::binary | ios::out);
    if (!f.is_open()){
      cout << "warning: failed to create bot input file: " << q2cstring(com_root + "/" + bot_buf.com_file) << endl;
      exit(-1);
    }else{
      f.write((const char*)&buf, 4);
      f.close();
    }

    bots[num_old + i] = bot_buf;
  }

  update_bot_list();
}

void snake::interface::update_bot_list(){
  int i;

  show_bots -> clear();

  for (int i = 0; i < bots.size(); i++){
    QIcon icon("data/snake-head-" + QString::number(i) + ".png");
    QListWidgetItem *lwi = new QListWidgetItem(icon, bots[i].exec_file, show_bots);
    show_bots -> addItem(lwi);
  }

  show_bots -> update();

  // new bots => no more replay
  input_forward -> setEnabled(false);
  input_backward -> setEnabled(false);
  
  input_start -> setEnabled(bots.size() > 1);
}

void snake::interface::update_rows(){
  QString v = input_rows -> text();
  rows = v.length() > 0 ? v.toInt() : 9;
  // image_buf -> check_size();
  layout() -> update();
  cout << "arena: set rows: " << rows << endl;
}

void snake::interface::update_cols(){
  QString v = input_cols -> text();
  cols = v.length() > 0 ? v.toInt() : 9;
  // image_buf -> check_size();
  layout() -> update();
  cout << "arena: set cols: " << cols << endl;
}

void snake::interface::setup(){
  QString v;

  update_rows();
  update_cols();
  
  map_buf.resize(rows * cols);
  memset(&map_buf[0], 0, rows * cols * sizeof(int));

  v = input_framerate -> text();
  frame_rate = v.length() > 0 ? v.toInt() : 2;
  
  for (int i = 0; i < bots.size(); i++){
    bots[i].path.clear();
    bots[i].direction = (snake::bot_direction)(rand() % (int)snake::num_directions);

    // find a place that does not lead to instant death
    bool ok = false;
    ptype x;
    int outcount = 0;

    // todo: fix initial positioning
    while (!ok){
      if (outcount++ > 1000){
	cout << "snake_interface: setup: failed to find initial position for bot " << i << endl;
	exit(-1);
      }

      x = ptype(init_margin + rand() % (rows - 2 * init_margin), init_margin + rand() % (cols - 2 * init_margin));
      ok = !map_buf[x.first + rows * x.second];
    }

    bots[i].row = x.first;
    bots[i].col = x.second;
    bots[i].action = snake::forward;
    bots[i].living = 1;
    bots[i].path.push_back(bots[i].get_path_data());
    map_buf[bots[i].row + rows * bots[i].col] = i + 1;
    cout << "setup bot at " << bots[i].row << "x" << bots[i].col << " as " << bots[i].living << endl;
  }

  cout << "resulting count:" << endl;
  count_bots();

  image_buf -> update();
}

void snake::interface::write_state(){
  /* state spec:
     pos : variable name : length
     ----------------------------
     ??  : game over     : 4
     ??  : next update   : 8
     ??  : num of bots   : 4
     ??  : bot_data[]    : 16 * (num of bots)
     ??  : map num rows  : 4
     ??  : map num cols  : 4
     ??  : map data      : 4 * rows * cols
   */
  int n = bots.size();
  fstream f(q2cstring(com_root + "/" + com_file), ios::binary | ios::out);

  if (!f.is_open()){
    cout << "warning: failed to create/open com file: " << q2cstring(com_root + "/" + com_file) << endl;
    exit(-1);
    return;
  }

  f.write((const char*)&game_over, 4);
  f.write((const char*)&next_epoch, 8);
  f.write((const char*)&n, 4);
  
  for (auto x : bots){
    f.write((const char*)&x.row, 16);
  }
  f.write((const char*)&rows, 4);
  f.write((const char*)&cols, 4);
  f.write((const char*)&map_buf[0], 4 * rows * cols);

  f.close();
}


void snake::interface::update_map(){
  int i, j;

  // move all living bots  
  for (i = 0; i < bots.size(); i++){
    if (bots[i].living){
      switch(bots[i].action){
      case snake::left:
	bots[i].direction = (snake::bot_direction)(((int)bots[i].direction + 3) % 4);
	break;
      case snake::right:
	bots[i].direction = (snake::bot_direction)(((int)bots[i].direction + 5) % 4);
	break;
      }

      switch (bots[i].direction){
      case snake::north:
	bots[i].row--;
	break;
      case snake::south:
	bots[i].row++;
	break;
      case snake::east:
	bots[i].col++;
	break;
      case snake::west:
	bots[i].col--;
	break;
      }
      
      bots[i].path.push_back(bots[i].get_path_data());
    }
  }

  // check all bots for crashes
  for (i = 0; i < bots.size(); i++){
    bots[i].living &= !(bots[i].row < 0 || bots[i].row >= rows || bots[i].col < 0 || bots[i].col >= cols);
    bots[i].living &= !map_buf[bots[i].row + rows * bots[i].col];
    for (j = 0; j < bots.size(); j++){
      bots[i].living &= j == i || bots[i].row != bots[j].row || bots[i].col != bots[j].col;
    }
  }

  // paint all bots
  for (i = 0; i < bots.size(); i++){
    if (bots[i].living){
      map_buf[bots[i].row + rows * bots[i].col] = i + 1;
    }
  }

  image_buf -> update();
}

int snake::interface::count_bots(){
  int sum = 0;
  cout << "count: ";
  for (auto x : bots){
    sum += x.living;
    cout << x.living << ", ";
  }
  cout << " = " << sum << endl;
  return sum;
}

void snake::interface::display_winner(){
  int id;

  switch(count_bots()){
  case 0:
    QMessageBox(QMessageBox::Information, "Game over", "Everyone died!").exec();
    break;
  case 1:
    id = -1;
    for (int i = 0; i < bots.size(); i++){
      id = bots[i].living ? i : id;
    }
    QMessageBox(QMessageBox::Information, "Game over", "The winner is: " + bots[id].exec_file).exec();
    break;
  default:
    QMessageBox(QMessageBox::Information, "Game over", "Ended prematurely!").exec();
  }
}

void snake::interface::abort(){
  game_over = true;
}

void snake::interface::start(){
  int i;
  QString com;
  int c;
  time_type t_start, t_span;
  char* v;
  int pid;

  if (bots.size() < 2){
    QMessageBox box;
    box.setText("Can not start with less than 2 bots!");
    box.exec();
    return;
  }

  setup();
  set_control_enables(false);
  input_abort -> setEnabled(true);
  cache_images();
  replay_max = 0;
  replay_step = 0;
  
  // set initial state in com buf
  game_over = 0;
  t_start = time_millis();
  next_epoch = t_start + (time_type)(1000 / frame_rate);
  write_state();

  // start bots
  for (i = 0; i < bots.size(); i++){
    // call by: botprogram input_file output_file id
    com = QString("./run_bot.sh ") + ts_masks[i] + " " + bots[i].exec_path + " " + com_root + "/" + com_file + " " + com_root + "/" + bots[i].com_file + " " + QString::number(i) + " &";
    system(q2cstring(com));
    cout << "started bot with command: " << com << endl;
  }

  // main loop
  while (count_bots() > 1 && !game_over){
    // wait for frame rate
    t_span = time_millis() - t_start;
    if ((time_type)(1000000 / frame_rate) > 1000 * t_span){
      cout << "waiting " << (((time_type)(1000000 / frame_rate)) - 1000 * t_span) << " microseconds ..." << endl;
      usleep(((time_type)(1000000 / frame_rate)) - 1000 * t_span);
    }else{
      cout << "snake_arena: WARNING: time overshoot: " << (time_millis() - next_epoch) << endl;
    }

    t_start = time_millis();
    next_epoch = t_start + (time_type)(1000 / frame_rate);

    cout << "main loop iteration: " << t_start << ", next epoch: " << next_epoch << endl;

    // update bot actions
    for (auto x = bots.begin(); x != bots.end(); x++){
      fstream f(q2cstring(com_root + "/" + x -> com_file), ios::binary | ios::in);
      if (!f.is_open()){
	cout << "warning: failed to open: " << q2cstring(com_root + "/" + x -> com_file) << endl;
      }else{ 
	f.read((char*)&i, 4);
	if (f.fail()){
	  cout << "warning: insufficient data in com file of bot " << q2cstring(x -> exec_file + ": " + com_root + "/" + x -> com_file) << endl;
	}else{
	  cout << "read from " << q2cstring(x -> exec_file + ": " + com_root + "/" + x -> com_file) << " action " << i << endl;
	  x -> action = (bot_action)i;
	}
	f.close();
      }
    }

    // increment and print
    replay_step++;
    update_map();
    write_state();
    image_buf -> repaint();
    QCoreApplication::processEvents();

  }

  input_abort -> setEnabled(false);

  cout << "displaying winner:" << endl;

  replay_max = replay_step;
  update_replay();

  game_over = 1;
  write_state();

  display_winner();

  // wait for unfinished bots
  for (auto x : bots){
    cout << "waiting for bot " << x.exec_file.toUtf8().data() << endl;
    setWindowTitle("Waiting for bot " + x.exec_file + " to finish...");
    while (get_pid(x.exec_file.toUtf8().data()) > -1){
      usleep(100000);
      QCoreApplication::processEvents();
    }
  }

  setWindowTitle(window_title);
  set_control_enables(true);
}

void snake::interface::update_replay(){
  display_step -> setText("replay: " + QString::number(replay_step));
  display_step -> update();
  image_buf -> update();
}

void snake::interface::step_forward(){
  replay_step = (replay_step + 1) % (replay_max + 1);
  update_replay();
}

void snake::interface::step_backward(){
  replay_step = (replay_step + replay_max) % (replay_max + 1);
  update_replay();
}

void snake::interface::closeEvent(QCloseEvent *e){
   system(q2cstring("rm " + com_root + "/*.scm " + com_root + "/" + com_file));
   QDialog::closeEvent(e);
}

void snake::interface::resizeEvent(QResizeEvent *e){
   QDialog::resizeEvent(e);
   cache_images();
}
