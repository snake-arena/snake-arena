#include <cstdlib>
#include <locale.h>
#include <QtGui>
#include "snake_interface.h"

int main(int argc, char **argv){
  QApplication app(argc, argv);

  // to get invertible string/number parsing
  setlocale(LC_NUMERIC, "C");
  srand(time(0));

  // read stylesheet
  QFile File("data/stylesheet.qss");
  File.open(QFile::ReadOnly);
  QString StyleSheet = QString::fromUtf8(File.readAll().data());
  app.setStyleSheet(StyleSheet);

  snake::interface iface;

  return iface.exec();
}
