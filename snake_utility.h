#ifndef SNAKE_ENUMS
#define SNAKE_ENUMS

namespace snake{
  enum bot_action{forward = 0, left, right, num_actions};
  enum bot_direction{north = 0, east, south, west, num_directions};

  struct bot_data{
    int row;
    int col;
    bot_direction direction;
    int living;
  };
  
};
#endif
