#ifndef SNAKE_BOT
#define SNAKE_BOT
#include <QString>
#include <list>
#include <utility>

#include "snake_utility.h"

namespace snake{
  struct path_data{
    int row;
    int col; 
    bot_direction dir;
  };

  struct bot{
    QString exec_path;
    QString exec_file;
    QString com_file;
    bot_action action;
    std::list<path_data> path;

    /* 'row' and below is hard coded for the com buffer */
    int row;
    int col;
    bot_direction direction;
    int living;

    path_data get_path_data();
  };
};
#endif
