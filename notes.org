* todo
** time for routines 
   is typically ~100 µs, with outliers ~10000 µs. Maybe this is a cpu
   scheduling issue?

   Running in real time seems to have solved the issue!
* bugs
** chooses wrong index
   only saw this happen once but .. why?
** snake_vis::heightForWidth does not work, do we want square field?
   hacked through resizeEvent -> resize recursion
* fixes
** apparently still fails to read pipe from ps
   the fork thingie didn't work, but going back to popen and
   remembering to pclose did the trick^^
** ulimit fails inconcistently
   using soft limit instead of hard, seems to work.
** clear widgets from qgroupbox (or any widget?)
   *Problem*: Deleting the layout does not clear the widgets.
   Deleting the widgets while the layout is installed bugs it somehow,
   so a call to layout() returns null, while setting a new layout
   maintains the old widgets at the same time as adding the new ones.
     
   *Solution*: Delete the layout first, then delete the child widgets. 
   
* bot ideas
** recursive with mc end step
** mc with neighbour logic
** detect camps?
