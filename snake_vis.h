#ifndef SNAKE_VIS
#define SNAKE_VIS
#include <QtGui>

namespace snake{
  class interface;

  class vis : public QLabel{
    Q_OBJECT
  public:
    vis(interface *parent);
    int heightForWidth(int w) const;
    int widthForHeight(int h) const;
    void check_size();

  private:
    interface *iface;
    void paintEvent(QPaintEvent *e);
    void resizeEvent(QResizeEvent *e); // force respect heightForWidth
  };
};

#endif
