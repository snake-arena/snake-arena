#ifndef SNAKE
#define SNAKE
#include <iostream>
#include <QtGui>
#include <chrono>

typedef unsigned long long int time_type;

// index 0 is for empty squares
const QColor color_player[5] = {
  QColor(40, 60, 80), // no player
  QColor(10, 30, 200),
  QColor(10, 200, 30),
  QColor(200, 30, 10),
  QColor(180, 20, 180)
};

const QColor color_grid(200, 180, 40);

std::ostream& operator << (std::ostream &stream, const QString &str);
const char* q2cstring(QString v);
time_type time_millis();
int get_pid(const char* pname);

#endif
